package practicano2;

import java.util.Scanner;

//@author Alejandra Soto

public class Ejercicio10while {

    public static void main(String[] args) {

        Scanner entrada = new Scanner(System.in);
        System.out.println("por favor ingresa un numero");

        int num = entrada.nextInt();
        int i = num;
        while (i >= 0) {
            System.out.print(i + "x");
            i--;
        }

    }

}
