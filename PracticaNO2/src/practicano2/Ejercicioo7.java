package practicano2;

import java.util.Scanner;

//@author Alejandra Soto

public class Ejercicioo7 {

    public static void main(String[] args) {

        Scanner entrada = new Scanner(System.in);

        int menor, medio, mayor;

        System.out.println("por favor ingrese el primer nombre");
        String nombre1 = entrada.next();
        System.out.println("por favor ingrese el segundo nombre ");
        String nombre2 = entrada.next();
        System.out.println("por  favor ingrese el tercer nombre");
        String nombre3 = entrada.next();

        int longitud1 = nombre1.length();
        int longitud2 = nombre2.length();
        int longitud3 = nombre3.length();
       
        if ((longitud1 <= longitud2) && (longitud1 <= longitud3)) {

            menor = longitud1;

            if (longitud2 <= longitud3) {
                medio = longitud2;
                mayor = longitud3;
            } else {
                medio = longitud3;
                mayor = longitud2;
            }
        } else if ((longitud2 <= longitud1) && (longitud2 < longitud3)) {

            menor = longitud2;

            if (longitud1 <= longitud3) {
                medio = longitud1;
                mayor = longitud3;
            } else {
                medio = longitud3;
                mayor = longitud1;
            }
        } else {
            menor = longitud3;

            if (longitud1 <= longitud2) {
                medio = longitud1;
                mayor = longitud2;
            } else {
                medio = longitud2;
                mayor = longitud1;
            }
        }
        System.out.println(menor + " " + medio + " " + mayor);
    }

}
