package practicano2;

//@author Alejandra Soto

public class Ejercicio9for {

    public static void main(String[] args) {
        
        int acum=0;
        for (int i = 1; i <= 10000; i++) {
            if(i%2==0){
               System.out.print(i + ","); 
            }else{
                acum = acum + i;
            }
        }
        System.out.println("");
        System.out.println("la suma de los numeros impares es: " + acum);
    }
}
