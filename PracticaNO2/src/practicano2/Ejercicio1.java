package practicano2;

import java.util.Scanner;

//@author Alejandra Soto

public class Ejercicio1 {

    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);

        Integer num = 4;
        double estructura = 1.5;
        float peso = 5;
        String nombre;
        boolean color = true;
    
        System.out.println("Por favor ingresa un nombre");
        nombre = entrada.next();
             
        System.out.println("Variable de tipo Integer: " + num);
        System.out.println("Variable de tipo Double: " + estructura);
        System.out.println("Variable de tipo float: " + peso);
        System.out.println("Variable de tipo String: " + nombre);
        System.out.println("Variable de tipo Boolean: " + color);
        
    }

}
